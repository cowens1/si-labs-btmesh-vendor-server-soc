/***************************************************************************//**
 * @file
 * @brief Core application logic.
 *******************************************************************************
 * # License
 * <b>Copyright 2022 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * SPDX-License-Identifier: Zlib
 *
 * The licensor of this software is Silicon Laboratories Inc.
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 ******************************************************************************/
#include "em_common.h"
#include "app_assert.h"
#include "sl_status.h"
#include "app.h"

#include "sl_btmesh_api.h"
#include "sl_bt_api.h"

#include "my_model_def.h"

#define UINT8_ARRAY_DUMP(array_base, array_size)                                                      \
  do                                                                                                  \
  {                                                                                                   \
    for (int i_log_exlusive = 0; i_log_exlusive < (array_size); i_log_exlusive++)                     \
    {                                                                                                 \
      app_log((i_log_exlusive + 1) % 16 ? "%02X " : "%02X\n", ((char *)(array_base))[i_log_exlusive]); \
    }                                                                                                 \
    app_log("\n");                                                                                     \
  } while (0)



/// Advertising Provisioning Bearer
#define PB_ADV                         0x1
/// GATT Provisioning Bearer
#define PB_GATT                        0x2


T_MY_MODEL myModel = {
    .elemIndex = PRIMARY_ELEMENT,
    .vendorId = MY_VENDOR_ID,
    .modelId = MY_MODEL_SERVER_ID,
    .publish = 1,
    .opcodesLen = NUM_MY_OP_CODES,
    .opcodesData[0] = my_op_get,
    .opcodesData[1] = my_op_set,
    .opcodesData[2] = my_op_status,
};

static void handle_vendor_model_receive(sl_btmesh_evt_vendor_model_receive_t *event);
static void send_counter_value(sl_btmesh_evt_vendor_model_receive_t *event);

/**************************************************************************//**
 * Application Init.
 *****************************************************************************/
SL_WEAK void app_init(void)
{
  /////////////////////////////////////////////////////////////////////////////
  // Put your additional application init code here!                         //
  // This is called once during start-up.                                    //
  /////////////////////////////////////////////////////////////////////////////
}

/**************************************************************************//**
 * Application Process Action.
 *****************************************************************************/
SL_WEAK void app_process_action(void)
{
  /////////////////////////////////////////////////////////////////////////////
  // Put your additional application code here!                              //
  // This is called infinitely.                                              //
  // Do not call blocking functions from here!                               //
  /////////////////////////////////////////////////////////////////////////////
}

/**************************************************************************//**
 * Bluetooth stack event handler.
 * This overrides the dummy weak implementation.
 *
 * @param[in] evt Event coming from the Bluetooth stack.
 *****************************************************************************/
void sl_bt_on_event(struct sl_bt_msg *evt)
{
  sl_status_t sc;
  switch (SL_BT_MSG_ID(evt->header)) {
    case sl_bt_evt_system_boot_id:
      // Initialize Mesh stack in Node operation mode,
      // wait for initialized event
#if 1 // set to 0 to clear provisioning data
      sc = sl_btmesh_node_init();
      app_assert_status_f(sc, "Failed to init node\n");
#else
      // clear provisioning data
      app_log("Clearing provision data.\n");
      sl_btmesh_node_reset();
#endif

      break;
    ///////////////////////////////////////////////////////////////////////////
    // Add additional event handlers here as your application requires!      //
    ///////////////////////////////////////////////////////////////////////////

    // -------------------------------
    // Default event handler.
    default:
      break;
  }
}

/**************************************************************************//**
 * Bluetooth Mesh stack event handler.
 * This overrides the dummy weak implementation.
 *
 * @param[in] evt Event coming from the Bluetooth Mesh stack.
 *****************************************************************************/
void sl_btmesh_on_event(sl_btmesh_msg_t *evt)
{
  sl_status_t sc;
  switch (SL_BT_MSG_ID(evt->header)) {
    case sl_btmesh_evt_node_initialized_id:
      app_log("BTMESH init event.\n");
      sc = sl_btmesh_vendor_model_init(PRIMARY_ELEMENT, myModel.vendorId, myModel.modelId, myModel.publish, myModel.opcodesLen, myModel.opcodesData);
      app_assert_status_f(sc, "Failed to init vendor model\n");
      if (!evt->data.evt_node_initialized.provisioned) {
        // The Node is now initialized,
        // start unprovisioned Beaconing using PB-ADV and PB-GATT Bearers
        app_log("Not provisioned, starting beaconing now.\n");
        sc = sl_btmesh_node_start_unprov_beaconing(PB_ADV | PB_GATT);
        app_assert_status_f(sc, "Failed to start unprovisioned beaconing\n");
      }
      else
      {
        app_log("Already provisioned!\n");
      }
      break;

    case sl_btmesh_evt_vendor_model_receive_id:
    {
      sl_btmesh_evt_vendor_model_receive_t *e;
      e = &evt->data.evt_vendor_model_receive;
      handle_vendor_model_receive(e);
    }
    break;

    ///////////////////////////////////////////////////////////////////////////
    // Add additional event handlers here as your application requires!      //
    ///////////////////////////////////////////////////////////////////////////

    // -------------------------------
    // Default event handler.
    default:
      break;
  }
}


static void handle_vendor_model_receive(sl_btmesh_evt_vendor_model_receive_t *event)
{
  app_log("Vendor model data received.\n\t"
         "Element index = %d\n\t"
         "Vendor id = 0x%04X\n\t"
         "Model id = 0x%04X\n\t"
         "Source address = 0x%04X\n\t"
         "Destination address = 0x%04X\n\t"
         "Destination label UUID index = 0x%02X\n\t"
         "App key index = 0x%04X\n\t"
         "Non-relayed = 0x%02X\n\t"
         "Opcode = 0x%02X\n\t"
         "Final = 0x%04X\n\t"
         "Payload: ",
         event->elem_index,
         event->vendor_id,
         event->model_id,
         event->source_address,
         event->destination_address,
         event->va_index,
         event->appkey_index,
         event->nonrelayed,
         event->opcode,
         event->final);
  UINT8_ARRAY_DUMP(event->payload.data, event->payload.len);
  app_log("\n");

  if (event->model_id == MY_MODEL_SERVER_ID)
  {
    switch (event->opcode)
    {
    case my_op_get:
      send_counter_value(event);
      break;

    default:
      // other opcodes have no other server functionality
      break;
    }
  }
}

static void send_counter_value(sl_btmesh_evt_vendor_model_receive_t *event)
{
  static uint32_t counter = 0;
  size_t payloadLen = 0;
  uint8_t payloadData[8] = {0};
  sl_status_t sc = SL_STATUS_OK;
  payloadData[0] = counter & 0xff;
  payloadData[1] = (counter >> 8) & 0xff;
  payloadData[2] = (counter >> 16) & 0xff;
  payloadData[3] = (counter >> 24) & 0xff;
  payloadLen = sizeof(counter);
  sc = sl_btmesh_vendor_model_send(event->source_address,
                                   event->va_index,
                                   event->appkey_index,
                                   myModel.elemIndex,
                                   myModel.vendorId,
                                   myModel.modelId,
                                   event->nonrelayed,
                                   my_op_status,
                                   1,
                                   payloadLen,
                                   payloadData);
  if (sc != SL_STATUS_OK)
  {
    app_log("[E: 0x%04x] sl_btmesh_vendor_model_send failed\n", (int)sc);
  }
  counter++;
}

