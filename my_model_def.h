#ifndef _MY_MODEL_DEF_H_
#define _MY_MODEL_DEF_H_
#include <stdint.h>

#define PRIMARY_ELEMENT 0
#define INFO_MODEL_ELEMENT 1
#define MY_VENDOR_ID 0x2FF
#define MY_MODEL_SERVER_ID 0x1111

#define NUM_MY_OP_CODES 3

typedef enum
{
  my_op_get = 0x11,
  my_op_set = 0x21,
  my_op_status = 0x31,
} E_MY_OP_CODES;

typedef struct
{
  uint16_t elemIndex;
  uint16_t vendorId;
  uint16_t modelId;
  uint8_t publish; // publish - 1, not - 0
  uint8_t opcodesLen;
  uint8_t opcodesData[NUM_MY_OP_CODES];
} T_MY_MODEL;

#endif
